package com.cognitivei.dvdmd;


import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.util.Joiner;
import com.google.api.client.util.Key;
import com.google.api.client.xml.XmlNamespaceDictionary;
import com.google.api.client.xml.XmlObjectParser;

import java.io.IOException;
import java.util.List;

public class TvDbSeries {

    private static final String API_KEY = "050880181E51339C";

    static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    HttpRequestFactory mRequestFactory;

    public static class TvDbSeriesResponse {
        @Key("id") public String seriesId;
        @Key("SeriesName") public String seriesName;
        @Key("Overview") public String overview;

        public String toString() {
            return "[ '" + seriesName + "', " + seriesId + " ]\n";
        }
    }

    public static class TvDbEpisodeResponse {

        @Key("id") public String episodeId;
        @Key("EpisodeName") public String title;
        @Key("EpisodeNumber") public int episode;
        @Key("SeasonNumber") public int season;

        @Key("DVD_discid") public int dvdDiscId;
        @Key("DVD_chapter") public int dvdChapter;

        public String toString() {
            return "[ " + season + "." + episode + ", '" + title + "', " + episodeId + ", "+ dvdDiscId + "." + dvdChapter + " ]\n";
        }

    }

    public static class TvDbResponse {
        @Key("Series") List<TvDbSeriesResponse> seriesList;
        @Key("Episode") List<TvDbEpisodeResponse> episodeList;

        public String toString() {
            return Joiner.on(',').join(seriesList);
        }
    }

    public TvDbSeries() {
        HttpRequestInitializer requestInitializer = new HttpRequestInitializer() {
            public void initialize(HttpRequest httpRequest) throws IOException {
                XmlNamespaceDictionary namespaceDictionary = new XmlNamespaceDictionary()
                        .set("","");
                httpRequest.setParser(new XmlObjectParser(namespaceDictionary));

            }
        };

        mRequestFactory = HTTP_TRANSPORT.createRequestFactory(requestInitializer);
    }


    public List<TvDbSeriesResponse> getShow(String title) throws IOException {
        GenericUrl url = new GenericUrl("http://thetvdb.com/api/GetSeries.php");
        url.put("seriesname", title);
        url.put("language", "en");

        HttpRequest req = mRequestFactory.buildGetRequest(url);
        HttpResponse response = req.execute();
        TvDbResponse thingie = response.parseAs(TvDbResponse.class);

        return thingie.seriesList;
    }


    public void getSeries(String showId) throws IOException {
        GenericUrl url = new GenericUrl("http://thetvdb.com/api/"+ API_KEY + "/series/" + showId + "/all");

        HttpRequest req = mRequestFactory.buildGetRequest(url);
        HttpResponse response = req.execute();
        TvDbResponse result = response.parseAs(TvDbResponse.class);

        System.out.println(result.episodeList);
    }

}
