package com.cognitivei;

import com.cognitivei.dvdmd.TvDbSeries;

import java.io.IOException;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        System.out.println("Hello World!");


        try {
            TvDbSeries tv = new TvDbSeries();
            List<TvDbSeries.TvDbSeriesResponse> shows = tv.getShow("Red Dwarf");
            System.out.println(shows);

            if(0 < shows.size()) {
                tv.getSeries(shows.get(0).seriesId);
            }


        } catch(IOException e) {
            System.err.println(e);
        }


    }
}
